#!/bin/bash

openstack_user_create() {
    /usr/bin/expect <<EOF
    set timeout 60
    spawn openstack user create --domain default --password-prompt myuser
    expect {
        "User Password:" { send "$MYROLE_PASS\r"; exp_continue}
        "Repeat User Password:" { send "$MYROLE_PASS\r"}
    }
    expect eof
EOF
}


source /usr/local/bin/admin-openrc

openstack domain create --description "An Example Domain" example
openstack project create --domain default --description "Service Project" service
openstack project create --domain default --description "Demo Project" myproject

# openstack user create --domain default --password-prompt myuser # interaction 
openstack_user_create
sleep 3s
openstack role create myrole
openstack role add --project myproject --user myuser myrole
