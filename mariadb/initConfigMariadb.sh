#!/bin/bash
mysql_install_db --user=mysql
mysqld_safe &
sleep 3s
mysqladmin -h 127.0.0.1 -u root password "$MARIADB_ROOT_PASSWD"
sleep 3s
mysql -uroot -p$MARIADB_ROOT_PASSWD -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY '$MARIADB_ROOT_PASSWD';"

mysql -uroot -p$MARIADB_ROOT_PASSWD -e "CREATE DATABASE keystone;GRANT ALL PRIVILEGES ON keystone.* TO 'keystone'@'localhost' IDENTIFIED BY '$KEYSTONE_DBPASS';GRANT ALL PRIVILEGES ON keystone.* TO 'keystone'@'%' IDENTIFIED BY '$KEYSTONE_DBPASS';"

sleep 3s

