#!/bin/bash

openstack_user_create() {
    /usr/bin/expect <<EOF
    set timeout 60
    spawn openstack user create --domain default --password-prompt myuser
    expect {
        "User Password:" { send "$MYROLE_PASS\r"; exp_continue}
        "Repeat User Password:" { send "$MYROLE_PASS\r"}
    }
    expect eof
EOF
}


#1, initkeystone.sh
sed -i "95c ServerName $KEYSTONE_IP" /etc/httpd/conf/httpd.conf
sed -i "598c connection = mysql+pymysql://keystone:$KEYSTONE_DBPASS@$MARIADB_IP/keystone" /etc/keystone/keystone.conf
ln -s /usr/share/keystone/wsgi-keystone.conf /etc/httpd/conf.d/

su -s /bin/sh -c "keystone-manage db_sync" keystone
sleep 3s
keystone-manage fernet_setup --keystone-user keystone --keystone-group keystone
sleep 3s

chmod 777 /etc/keystone/credential-keys
keystone-manage credential_setup --keystone-user keystone --keystone-group keystone

sleep 3s

keystone-manage bootstrap --bootstrap-password $ADMIN_PASS --bootstrap-admin-url http://$KEYSTONE_IP:5000/v3/ --bootstrap-internal-url http://$KEYSTONE_IP:5000/v3/ --bootstrap-public-url http://$KEYSTONE_IP:5000/v3/ --bootstrap-region-id RegionOne
# 2,httpdStart.sh
/usr/sbin/httpd -DFOREGROUND

## 3,
#source /usr/local/bin/admin-openrc
#
## 4,roleCreate.sh
#openstack domain create --description "An Example Domain" example
#openstack project create --domain default --description "Service Project" service
#openstack project create --domain default --description "Demo Project" myproject
# openstack user create --domain default --password-prompt myuser # interaction 
#openstack_user_create
#sleep 3s
#openstack role create myrole
#openstack role add --project myproject --user myuser myrole
##/usr/sbin/httpd  -DFOREGROUND
